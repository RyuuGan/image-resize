'use strict';

const gm = require('gm').subClass({ imageMagick: true });

module.exports = gm;
