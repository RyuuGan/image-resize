'use strict';

const mongoose = require('mongoose')
  , utils = require('../utils')
  , gm = require('../gm')
  , conf = require('../conf')
  , request = require('request')
  , fs = require('fs-extra')
  , path = require('path');

let Upload = mongoose.Schema({

  url: {
    type: String
  },

  filename: {
    type: String,
    required: true
  },

  md5: {
    type: String,
    required: true
  },

  createdAt: {
    type: Date,
    default: Date.now
  }

});

Upload.plugin(require('./plugins/storage'));
Upload.plugin(require('./plugins/extend'));

Upload.virtual('client').get(function () {
  return {
    url: this.url,
    filename: this.filename,
    size: this.size,
    files: this.getFiles()
  };
});

Upload.virtual('dirname').get(function () {
  return 'uploads/' + this.id; // later on use pathFromId
});

Upload.statics.processUrl = async function (url) {
  let Model = this;
  return new Promise((resolve, reject) => {
    let filename = path.join(conf.storageRoot, 'tmp', utils.randomString(32));
    request
      .get(url)
      .on('response', response => {
        if (response.statusCode !== 200) {
          return reject(response);
        }
        if (response.headers['content-type'].indexOf('image/') !== 0) {
          return reject(response);
        }
      })
      .on('end', () => resolve(filename))
      .pipe(fs.createWriteStream(filename));
  }).then(filename => {
    let originalname = url.substring(url.lastIndexOf('/') + 1);
    let idx = originalname.indexOf('#');
    if (idx !== -1) {
      originalname = originalname.substring(0, idx);
    }
    idx = originalname.indexOf('?');
    if (idx !== -1) {
      originalname = originalname.substring(0, idx);
    }
    return Model.process({ path: filename, originalname }, url);
  });
};

Upload.statics.process = async function (file, url) {
  let Model = this;

  let md5 = await utils.md5File(file.path);

  let upload = await Model.findOne({ md5: md5 });

  if (upload) {
    return upload;
  }

  let uploaded = new Model({
    url: url,
    filename: file.originalname,
    md5: md5
  });
  await uploaded.mkdirp('');
  await uploaded.resize(file);

  return await uploaded.save();
};

Upload.methods.resize = async function (file) {
  let upload = this;
  for (let i = 0; i < conf.sizes.length; i++) {
    await upload.gm(file, conf.sizes[i]);
  }
};

Upload.methods.gm = async function (file, size) {
  let upload = this;
  return new Promise((resolve, reject) => {
    gm(file.path)
      .resize(...size)
      .write(upload.getFile(`${size.join('x')}.png`), err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
  });
};

Upload.methods.getFiles = function () {
  return conf.sizes.map(s => {
    let size = s.join('x');
    return {
      size,
      url: `/storage/${this.dirname}/${size}.png`
    };
  });
};

module.exports = exports = mongoose.model('Upload', Upload);
