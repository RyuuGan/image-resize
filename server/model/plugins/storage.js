'use strict';

const path = require('path')
  , conf = require('../../conf')
  , utils = require('../../utils')
  , fs = require('fs-extra');

module.exports = exports = function (schema) {

  schema.plugin(require('./pathFromId'));

  schema.virtual('storageRoot').get(function () {
    return path.join(conf.storageRoot, this.dirname);
  });

  schema.methods.getFile = function (relPath) {
    return path.join(this.storageRoot, utils.normalizePath(relPath));
  };

  schema.methods.mkdirp = async function (relPath) {
    let dir = this.getFile(relPath);
    await fs.ensureDir(dir);
    return dir;
  };

  schema.methods.rmFile = async function (relPath) {
    let file = this.getFile(relPath);
    await fs.remove(file);
    await utils.cleanEmptyDirs(path.dirname(file));
  };

  schema.methods.sendFile = async function (res, path, attachment) {
    if (attachment)
      res.attachment(attachment);
    res.set('X-Accel-Redirect', '/storage/' + this.dirname + '/' + path);
    res.end();
  };

};
