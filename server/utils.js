'use strict';

const crypto = require('crypto')
  , md5 = require('md5-file/promise')
  , stringex = require('stringex')
  , fs = require('fs')
  , path = require('path');

exports.sha256 = (str) => {
  let p = crypto.createHash('sha256');
  p.update(str, 'utf-8');
  return p.digest('hex');
};

exports.sha512 = (str) => {
  let p = crypto.createHash('sha512');
  p.update(str, 'utf-8');
  return p.digest('hex');
};

exports.md5 = (str) => {
  let p = crypto.createHash('md5');
  p.update(str, 'utf-8');
  return p.digest('hex');
};

exports.md5File = async (path) => {
  return await md5(path); // returns hash
};

exports.normalizePath = function (file) {
  return stringex.toASCII(path.normalize(file))
    .replace(/[^\/a-z0-9._-]/gi, '_');
};

exports.randomString = (length) => {
  let CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length; i++)
    result += CHARS[Math.floor(Math.random() * CHARS.length)];
  return result;
};

exports.cleanEmptyDirs = function (dir, cb) {
  fs.readdir(dir, function (err, files) {
    if (err) return cb();
    if (files.length) return cb();
    fs.rmdir(dir, function (err) {
      if (err) return cb();
      exports.cleanEmptyDirs(path.dirname(dir), cb);
    });
  });
};
