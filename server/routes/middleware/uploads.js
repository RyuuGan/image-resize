'use strict';

const conf = require('../../conf')
  , utils = require('../../utils')
  , multer = require('multer')
  , path = require('path');

/**
 * File upload helper
 */
module.exports = exports = function () {

  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(conf.storageRoot, 'tmp'));
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + utils.randomString(4) + '-' + Date.now())
    }
  });

  return multer({
    storage: storage,
    limits: {
      fileSize: conf.uploadFileSizeLimit
    }
  }).any();

};
