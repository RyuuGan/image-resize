'use strict';

const path = require('path')
  , conf = require('../../../conf');

module.exports.uploaded = function (req, res, next) {
  if (!req.files) {
    return res.apiFailed('UPLOAD_FILE_NOT_UPLOADED',
      'File is required for upload.');
  }

  let file = req.files.file;

  if (!file) {
    return res.apiFailed('UPLOAD_FILE_NOT_UPLOADED',
      'File is required for upload.');
  }

  next();

};

module.exports.image = function (req, res, next) {
  if (!req.files) {
    return res.apiFailed('UPLOAD_FILE_NOT_UPLOADED',
      'File is required for upload.');
  }

  let file = req.files.file;

  if (!file) {
    return res.apiFailed('UPLOAD_FILE_NOT_UPLOADED',
      'File is required for upload.');
  }

  let ext = path.extname(file.originalname).toLowerCase().substring(1);

  if (conf.imgExtensions.indexOf(ext) === -1) {
    return res.apiFailed('UPLOAD_FILE_NOT_IMAGE',
      'Uploaded file have unsupported image extension.');
  }

  next();
};
