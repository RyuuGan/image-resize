'use strict';

let $ = require('./index')
  , wrap = require('express-async-wrap')
  , Upload = require('../../../../model/upload');

let PREFIX = '/url';

$.post(PREFIX, wrap(async (req, res) => {

  let upload = await Upload.processUrl(req.body.url);

  res.apiSuccess({
    files: upload.getFiles()
  });

}));
