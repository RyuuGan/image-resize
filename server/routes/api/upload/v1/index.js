'use strict';

const express = require('express');

let router = module.exports = new express.Router();

router.use(require('../../../middleware/uploads')());

/**
 * Parsing files adding `req.files.<fieldname>` to `req.files` if exists.
 */
router.use(function (req, res, next) {
  if (req.files && req.files.length) {
    req.files.forEach(function (item) {
      let file = req.files[item.fieldname];
      if (!file) {
        req.files[item.fieldname] = item;
      } else if (!Array.isArray(file)) {
        req.files[item.fieldname] = [req.files[item.fieldname]];
        req.files[item.fieldname].push(item);
      } else {
        req.files[item.fieldname].push(item);
      }
    });
  }
  next();
});

require('./file');
require('./url');
