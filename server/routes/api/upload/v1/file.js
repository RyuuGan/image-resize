'use strict';

let $ = require('./index')
  , wrap = require('express-async-wrap')
  , { uploaded } = require('../_middleware')
  , Upload = require('../../../../model/upload');

let PREFIX = '/file';

$.post(PREFIX, uploaded, wrap(async (req, res, next) => {

  let upload = await Upload.process(req.files.file);

  res.apiSuccess({
    files: upload.getFiles()
  });

}));
