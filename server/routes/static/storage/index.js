'use strict';

const express = require('express')
  , Upload = require('../../../model/upload')
  , wrap = require('express-async-wrap')
  , fs = require('fs-extra');

const $ = module.exports = new express.Router();

const PREFIX = '/storage';

$.get(PREFIX + '/uploads/:id/:filename', wrap(async function (req, res, next) {

  let upload = await Upload.findOne({
    _id: req.params.id
  });

  if (!upload) return res.sendStatus(404);

  let file = upload.getFile(req.params.filename);

  let stat = await fs.stat(file);

  if (!stat || !stat.isFile())
    return res.sendStatus(404);

  res.download(file, `${upload.id}–${req.params.filename}`);

}));
