# Crypto clients

## Mac OS X Build Instructions and Notes

Preparation
-----------
Install the OS X command line tools:

`xcode-select --install`

When the popup appears, click `Install`.

Then install [Homebrew](https://brew.sh).


### Bitcoin, Litecoin, Dash


Dependencies
----------------------

    brew install automake berkeley-db4 libtool boost --c++11 miniupnpc openssl pkg-config protobuf python3 libevent

Build Core
------------------------

1. Clone the source code and cd into required folder

        git clone https://github.com/bitcoin/bitcoin
        cd bitcoin

        git clone https://github.com/litecoin-project/litecoin
        cd litecoin

        git clone hhttps://github.com/dashpay/dash
        cd dash

2.  Build:
        ./autogen.sh
        ./configure --without-gui
        make

3.  It is recommended to build and run the unit tests:

        make check


### DogeCoin

Dependencies
----------------------

    brew install automake libtool boost --c++11 miniupnpc openssl pkg-config protobuf python3 libevent

    brew install berkeley-db # You need to make sure you install a version >= 5.1.29

    Install berkeley-db

    cd setup
    unzip db-5.1.29.zip
    cd db-5.1.29/build_unix
    sh ../dist/configure --enable-cxx --prefix=/usr/local
    make
    make install

Build Core
------------------------

1. Clone the dogecoin source code and cd into `dogecoin`

        git clone https://github.com/dogecoin/dogecoin
        cd dogecoin

2.  Build:

        vim build-aux/m4/bitcoin_find_bdb51.m4

        set bdbdirlist
        bdbdirlist=../db-5.1.29/build_unix    TODO

        ./autogen.sh
        export LDFLAGS="-L/usr/local/opt/openssl/lib -L/usr/local/BerkeleyDB.5.1/lib"
        export CPPFLAGS="-I/usr/local/opt/openssl/include -I/usr/local/BerkeleyDB.5.1/include"
        ./configure --without-gui  --enable-wallet
        make

3.  It is recommended to build and run the unit tests:

        make check



