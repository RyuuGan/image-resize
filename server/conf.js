'use strict';

const Configuration = require('iconf');

let conf = module.exports = exports = new Configuration({

  root: process.cwd(),

  ip: {
    production: '127.0.0.1',
    development: ''
  },

  locales: ['ru'],

  port: '3031',

  host: {
    development: '127.0.0.1',
    production: '' // todo
  },

  clientHost: {
    development: '127.0.0.1:3030',
    production: '' // todo
  },

  secured: false,

  uploadFileSizeLimit: 5 * 1024 * 1024,

  imgExtensions: ['png', 'jpg', 'jpeg'],

  sizes: [
    [1280, 720],
    [720, 480],
    [480, 320]
  ],

  mongo: {
    url: 'mongodb://127.0.0.1/ir'
  },

  storageRoot: '/var/ir',

  workers: {
    development: 1,
    production: 1  //todo
  },

  development: {

    errorHandler: {
      dumpExceptions: true,
      showStack: true
    },

    loggerOptions: 'dev'

  }

});

Object.defineProperty(conf, 'origin', {
  get: function () {
    return '//' + this.host;
  }
});

Object.defineProperty(conf, 'protocol', {
  get: function () {
    return this.secured ? 'https' : 'http';
  }
});

Object.defineProperty(conf, 'clientOrigin', {
  get: function () {
    return this.protocol + '://' + this.clientHost;
  }
});
