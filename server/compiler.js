'use strict';

const rho = require('rho');

// Empty rho compiler
module.exports = new rho.AsyncCompiler({
  resolveLink: function (/* id */) {
    return null;
  },
  resolveImage: function (id) {
    return this.resolveLink(id);
  }
});
