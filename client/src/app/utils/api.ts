'use strict';

import conf from '../conf';

// import AuthService from '../services/scim.service';

/**
 * Helper for construction api URLs
 */
class ApiHelper {
  static version = 'v1';
  static secured: boolean = conf.secured;

  static makeUrl(endpoint: string, prefix = ''): string {
    const _endpoint = endpoint.replace(/^\//, '');
    const _prefix = prefix ? prefix.replace(/^\//, '') + '/' : '';
    return conf.apiOrigin + '/api/' + _prefix + ApiHelper.version + '/' + _endpoint;
  }

  static upload(endpoint: string): string {
    return ApiHelper.makeUrl(endpoint, 'upload');
  }

  static scim(endpoint: string): string {
    return ApiHelper.makeUrl(endpoint, 'scim');
  }

}

export default ApiHelper;
