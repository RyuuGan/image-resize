class Conf {
  production = false;
  test = false;
  locales: string[] = ['en'];
  apiHost = '127.0.0.1:3031';
  host = '127.0.0.1:3030';
  secured = false;

  get apiOrigin() {
    return this.secured ? 'https://' + this.apiHost : 'http://' + this.apiHost;
  }

  get origin() {
    return this.secured ? 'https://' + this.host : 'http://' + this.host;
  }

}

export default new Conf();
