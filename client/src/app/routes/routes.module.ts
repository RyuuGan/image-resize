import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { PrincipalResolver } from '../services/principal.resolver';
import { HomeComponent } from './home';
import { FileUploadModule } from 'ng2-file-upload';
import { SharedModule } from '../sharedModule';

let routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    SharedModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    HomeComponent
  ],
  providers: [
    PrincipalResolver
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
