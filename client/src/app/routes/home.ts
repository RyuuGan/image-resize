import { Component } from '@angular/core';
import {
  FileItem, FileLikeObject, FileUploader,
  ParsedResponseHeaders
} from 'ng2-file-upload';
import { NotificationsService } from 'angular2-notifications/dist';
import { TranslateService } from '@ngx-translate/core';
import conf from '../conf';
import { Http } from '@angular/http';
import ApiHelper from '../utils/api';

@Component({
  selector: 'cc-home',
  templateUrl: './home.html',
  styleUrls: ['./home.scss']
})
export class HomeComponent {

  // FileUploader

  public uploader: FileUploader = new FileUploader({
    url: ApiHelper.upload('file'),
    filters: [{
      name: 'imageFilter',
      fn: function (item: FileLikeObject) {
        return item.type.substring(0, item.type.indexOf('/')) === 'image';
      }
    }],
    queueLimit: 1
  });
  public hasBaseDropZoneOver = false;
  fileUrl: string;
  selectedUrl = '';

  uploadedFiles: string[] = [];

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  constructor(private notifications: NotificationsService,
              private translate: TranslateService,
              private http: Http) {
    let $this = this;

    this.uploader.onAfterAddingFile = function (item: FileItem) {
      let reader = new FileReader();

      reader.onload = function (event: any) {
        $this.fileUrl = event.target.result;
      };

      reader.readAsDataURL(item._file);
    };

    this.uploader.onCompleteItem = function (_item: FileItem, res: string, status: number, _headers: ParsedResponseHeaders) {
      if (status === 200) {
        let body;
        try {
          body = JSON.parse(res);
        } catch (e) {
          body = null;
        }

        if (body && !body.error) {
          $this.notifications.success('', $this.translate.instant('Файл успешно обработан.'));
          $this.uploadedFiles = body.results.files;
        } else {
          $this.error();
        }
      } else {
        $this.error();
      }

    };

    this.uploader.onCompleteAll = function () {
    };
  }

  upload() {
    this.uploader.uploadAll();
  }

  cancelUpload() {
    this.uploader.clearQueue();
  };

  uploadUrl() {
    if (!this.selectedUrl) {
      return;
    }
    this.http
      .post(ApiHelper.upload('url'), { url: this.selectedUrl })
      .subscribe(data => {
        let _data = data.json();
        if (_data && !_data.error) {
          this.notifications.success('', this.translate.instant('Файл успешно обработан.'));
          this.uploadedFiles = _data.results.files;
        } else {
          this.error();
        }
      });
  }

  error() {
    this.uploadedFiles = [];
    this.fileUrl = '';
    this.cancelUpload();
    this.notifications.error('', this.translate.instant('Файл не обработан.'));
  }

  getUrl(url: string) {
    return conf.apiOrigin + url;
  }

}
